INSERT INTO drone (id, serial_number, model, weight_limit, battery_capacity, state)
VALUES
       (1, 'SERIAL_001', 'LIGHT_WEIGHT', 100.0, 100, 'IDLE'),
       (2, 'SERIAL_002', 'LIGHT_WEIGHT', 100.0, 90, 'IDLE'),
       (3, 'SERIAL_003', 'MIDDLE_WEIGHT', 200.0, 80, 'IDLE'),
       (4, 'SERIAL_004', 'MIDDLE_WEIGHT', 200.0, 15, 'IDLE'),
       (5, 'SERIAL_005', 'CRUISE_WEIGHT', 350.5, 100, 'IDLE'),
       (6, 'SERIAL_006', 'CRUISE_WEIGHT', 350.5, 100, 'IDLE'),
       (7, 'SERIAL_007', 'HEAVY_WEIGHT', 500.0, 98, 'IDLE'),
       (8, 'SERIAL_008', 'HEAVY_WEIGHT', 500.0, 90, 'IDLE'),
       (9, 'SERIAL_009', 'LIGHT_WEIGHT', 50.1, 10, 'IDLE'),
       (10, 'SERIAL_010', 'LIGHT_WEIGHT', 10, 10, 'IDLE');





