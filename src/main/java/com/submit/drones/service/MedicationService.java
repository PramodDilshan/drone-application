package com.submit.drones.service;

import com.submit.drones.model.Medication;
import com.submit.drones.repository.MedicationRepository;
import com.submit.drones.util.Const;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MedicationService {

    private static final Logger LOGGER = LogManager.getLogger(MedicationService.class);

    @Autowired
    MedicationRepository medicationRepository;


    public List<Medication> saveAll(List<Medication> medications){
        LOGGER.info(Const.Log.BRACKETS_4, Const.ActionType.SAVE_ALL, Const.Log.START, Const.Log.MEDICATIONS, medications);
        List<Medication> savedMedications =  medicationRepository.saveAll(medications);
        LOGGER.info(Const.Log.BRACKETS_5, Const.ActionType.SAVE_ALL, Const.Log.END, Const.Log.STATUS_SUCCESS, Const.Log.MEDICATIONS, savedMedications);
        return savedMedications;
    }
}
