package com.submit.drones.service;

import com.submit.drones.model.Drone;
import com.submit.drones.model.DroneBatteryLog;
import com.submit.drones.repository.DroneBatteryLogRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DroneBatteryLogService {

    @Autowired
    DroneBatteryLogRepository droneBatteryLogRepository;



    public DroneBatteryLog save(Drone drone) {
        return droneBatteryLogRepository.save(new DroneBatteryLog(drone, drone.getBatteryCapacity()));
    }
}
