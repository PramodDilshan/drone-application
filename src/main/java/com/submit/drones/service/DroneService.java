package com.submit.drones.service;

import com.submit.drones.dto.MedicationDTO;
import com.submit.drones.exception.BatteryException;
import com.submit.drones.exception.DroneException;
import com.submit.drones.model.Drone;
import com.submit.drones.model.Medication;
import com.submit.drones.repository.DroneRepository;
import com.submit.drones.util.Const;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

@Service
public class DroneService {

    private static final Logger LOGGER = LogManager.getLogger(DroneService.class);

    @Value("${minimum-battery-level}")
    private int MINIMUM_BATTERY_LEVEL;

    @Autowired
    private DroneRepository droneRepository;

    @Autowired
    private DroneBatteryLogService droneBatteryLogService;

    @Autowired
    private MedicationService medicationService;

    public Drone save(Drone drone){
        LOGGER.info(Const.Log.BRACKETS_4, Const.ActionType.SAVE, Const.Log.START, Const.Log.DRONE, drone);
        Drone savedDrone =  droneRepository.save(drone);
        LOGGER.info(Const.Log.BRACKETS_5, Const.ActionType.SAVE, Const.Log.END, Const.Log.STATUS_SUCCESS , Const.Log.DRONE, savedDrone);
        return savedDrone;
    }

    public Drone update(Drone drone){
        LOGGER.info(Const.Log.BRACKETS_4, Const.ActionType.UPDATE, Const.Log.START, Const.Log.DRONE, drone);
        if(drone.getId() != 0 && droneRepository.existsById(drone.getId())){
            Drone updatedDrone =  droneRepository.save(drone);
            LOGGER.info(Const.Log.BRACKETS_5, Const.ActionType.UPDATE, Const.Log.END, Const.Log.STATUS_SUCCESS ,Const.Log.DRONE, updatedDrone);
            return updatedDrone;
        }
        else {
            LOGGER.error(Const.Log.BRACKETS_4, Const.ActionType.UPDATE, Const.Log.END, Const.Log.STATUS_FAILED, Const.ErrorMessages.NO_DRONES_ARE_FOUND);
            throw new DroneException(Const.ErrorMessages.NO_DRONES_ARE_FOUND);
        }
    }

    public List<Drone> findAll() {
        LOGGER.info(Const.Log.BRACKETS_2, Const.ActionType.FIND_ALL, Const.Log.START);
        List<Drone> drones =  droneRepository.findAll();
        LOGGER.info(Const.Log.BRACKETS_5, Const.ActionType.FIND_ALL, Const.Log.END, Const.Log.STATUS_SUCCESS , Const.Log.DRONES, drones);
        return drones;
    }

    public List<MedicationDTO> load(long droneId, List<Medication> medications, MultipartFile[] images) {
        LOGGER.info(Const.Log.BRACKETS_4, Const.ActionType.LOAD, Const.Log.START, Const.Log.MEDICATIONS, medications);
        Drone drone = droneRepository.findById(droneId).orElseThrow(() -> {
            LOGGER.error(Const.Log.BRACKETS_4, Const.ActionType.LOAD, Const.Log.END, Const.Log.STATUS_FAILED, Const.ErrorMessages.NO_DRONES_ARE_FOUND);
            throw new DroneException((Const.ErrorMessages.NO_DRONES_ARE_FOUND));
        });
        LOGGER.info(Const.Log.BRACKETS_4, Const.ActionType.LOAD, Const.Log.STATUS_SUCCESS, Const.Log.DRONE, drone);
        if(drone.getBatteryCapacity() >= MINIMUM_BATTERY_LEVEL){
            updateState(drone, Const.State.LOADING);
            List<Medication> savedMedications = medicationService.saveAll(validateForWeightCapacityAndAttachImage(drone, medications, images));
            LOGGER.info(Const.Log.BRACKETS_4, Const.ActionType.LOAD, Const.Log.STATUS_SUCCESS, Const.Log.MEDICATIONS, medications);
            updateState(drone, Const.State.LOADED);
            LOGGER.info(Const.Log.BRACKETS_5, Const.ActionType.LOAD, Const.Log.END, Const.Log.STATUS_SUCCESS ,Const.Log.MEDICATIONS, savedMedications);
            return mapMedicationDTOListWithLoadedStatus(medications, savedMedications);
        }
        else {
            LOGGER.error(Const.Log.BRACKETS_4, Const.ActionType.LOAD, Const.Log.END, Const.Log.STATUS_FAILED, Const.ErrorMessages.PLEASE_CHARGE_THE_BATTERY_BEFORE_LOADING);
            throw new BatteryException();
        }

    }

    private Drone updateState(Drone drone, Const.State state) {
        LOGGER.info(Const.Log.BRACKETS_6, Const.ActionType.UPDATE_STATE, Const.Log.START, Const.Log.DRONE, drone, Const.Log.START, state);
        drone.setState(state);
        Drone updatedDrone =  update(drone);
        LOGGER.info(Const.Log.BRACKETS_5, Const.ActionType.UPDATE_STATE, Const.Log.END, Const.Log.STATUS_SUCCESS ,Const.Log.DRONE, drone);
        return updatedDrone;
    }

    private List<Medication> validateForWeightCapacityAndAttachImage(Drone drone, List<Medication> medications, MultipartFile[] files){
        LOGGER.info(Const.Log.BRACKETS_6, Const.ActionType.VALIDATE_MAX_WEIGHT, Const.Log.START, Const.Log.MEDICATIONS, medications, Const.Log.DRONE, drone);
        AtomicReference<Float> totalWight = new AtomicReference<>((float) 0);
        drone.getLoad().forEach(medication -> totalWight.updateAndGet(value -> value + medication.getWeight()));
        List<Medication> loadedMedications = medications.stream()
                .filter(medication -> drone.getWeightLimit() >= totalWight.get() + medication.getWeight())
                .peek(medication -> {
                    totalWight.updateAndGet(value -> value + medication.getWeight());
                    medication.setDrone(drone);
                    try {
                        medication.setImage(Arrays.stream(files).iterator().next().getBytes());
                    } catch (Exception e) {
                        LOGGER.info(Const.Log.BRACKETS_3, Const.ActionType.SET_IMAGES_TO_MEDICATIONS, Const.Log.STATUS_FAILED , e.getMessage());
                    }
                })
                .collect(Collectors.toList());
        LOGGER.info(Const.Log.BRACKETS_5, Const.ActionType.VALIDATE_MAX_WEIGHT, Const.Log.END, Const.Log.STATUS_SUCCESS , Const.Log.MEDICATIONS, loadedMedications);
        return loadedMedications;
    }

    private List<MedicationDTO> mapMedicationDTOListWithLoadedStatus(List<Medication> medications, List<Medication> savedMedications) {
        return medications.stream().map(medication -> savedMedications.stream()
                .filter(savedMedication -> medication.getCode().equalsIgnoreCase(savedMedication.getCode()))
                .map(savedMedication -> new MedicationDTO(
                        savedMedication.getId(),
                        savedMedication.getName(),
                        savedMedication.getWeight(),
                        savedMedication.getCode(),
                        savedMedication.getImage(),
                        Const.LoadedStatus.SUCCESS))
                .findFirst()
                .orElse(new MedicationDTO(
                        medication.getId(),
                        medication.getName(),
                        medication.getWeight(),
                        medication.getCode(),
                        medication.getImage(),
                        Const.LoadedStatus.FAILED))).collect(Collectors.toList());
    }

    public List<Medication> findAllLoads(long droneId) {
        LOGGER.info(Const.Log.BRACKETS_4, Const.ActionType.FIND_ALL_LOADS, Const.Log.START, Const.Log.DRONE_ID, droneId);
        List<Medication> medications =  droneRepository.findById(droneId).orElseThrow(() -> {
            LOGGER.info(Const.Log.BRACKETS_4, Const.ActionType.FIND_ALL_LOADS, Const.Log.END, Const.Log.STATUS_FAILED, Const.ErrorMessages.NO_MEDICATIONS_FOUND);
            throw new DroneException(Const.ErrorMessages.NO_MEDICATIONS_FOUND);
        }).getLoad();
        LOGGER.info(Const.Log.BRACKETS_5, Const.ActionType.FIND_ALL_LOADS, Const.Log.END, Const.Log.STATUS_SUCCESS, Const.Log.MEDICATIONS, medications);
        return medications;
    }

    public List<Drone> findAllByState(Const.State state) {
        LOGGER.info(Const.Log.BRACKETS_4, Const.ActionType.FIND_ALL_BY_STATE, Const.Log.START, Const.Log.STATE, state);
        List<Drone> drones =  droneRepository.findByState(state).orElseThrow(() -> {
            LOGGER.info(Const.Log.BRACKETS_4, Const.ActionType.FIND_ALL_BY_STATE, Const.Log.END, Const.Log.STATUS_FAILED, Const.ErrorMessages.NO_DRONES_ARE_FOUND);
            throw new DroneException(Const.ErrorMessages.NO_DRONES_ARE_FOUND);
        });
        LOGGER.info(Const.Log.BRACKETS_5, Const.ActionType.FIND_ALL_BY_STATE, Const.Log.END, Const.Log.STATUS_SUCCESS, Const.Log.DRONES, drones);
        return drones;
    }

    public int getBatteryLevel(long droneId) {
        LOGGER.info(Const.Log.BRACKETS_4, Const.ActionType.GET_BATTERY_LEVEL, Const.Log.START, Const.Log.DRONE_ID, droneId);
        int batteryLevel =  droneRepository.findById(droneId).orElseThrow(() -> {
            LOGGER.info(Const.Log.BRACKETS_3, Const.ActionType.GET_BATTERY_LEVEL, Const.Log.END, Const.Log.STATUS_FAILED);
            throw new DroneException(Const.ErrorMessages.NO_DRONES_ARE_FOUND);
        }).getBatteryCapacity();
        LOGGER.info(Const.Log.BRACKETS_5, Const.ActionType.GET_BATTERY_LEVEL, Const.Log.END, Const.Log.STATUS_SUCCESS, Const.Log.DRONE_ID, droneId);
        return batteryLevel;
    }

    public void logBatterLevel(){
        droneRepository.findAll().forEach(drone -> droneBatteryLogService.save(drone));
    }
}
