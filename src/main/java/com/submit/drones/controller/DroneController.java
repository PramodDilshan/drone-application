package com.submit.drones.controller;

import com.submit.drones.dto.MedicationDTO;
import com.submit.drones.dto.MedicationListDTO;
import com.submit.drones.model.Drone;
import com.submit.drones.model.Medication;
import com.submit.drones.service.DroneService;
import com.submit.drones.util.Const;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.util.List;


@Validated
@RestController
@RequestMapping("/drone")
public class DroneController {

    private static final Logger LOGGER = LogManager.getLogger(DroneController.class);


    @Autowired
    private DroneService droneService;

    @GetMapping
    public @ResponseBody List<Drone> findAll(){
        LOGGER.info(Const.Log.BRACKETS_2, Const.ActionType.FIND_ALL_DRONES, Const.Log.START);
        List<Drone> drones = droneService.findAll();
        LOGGER.info(Const.Log.BRACKETS_5, Const.ActionType.FIND_ALL_DRONES, Const.Log.END, Const.Log.STATUS_SUCCESS, Const.Log.DRONES, drones);
        return drones;
    }

    @PostMapping
    public @ResponseBody Drone create(@RequestBody @Valid Drone drone){
        LOGGER.info(Const.Log.BRACKETS_2, Const.ActionType.CREATE_DRONE, Const.Log.START);
        Drone createdDrone = droneService.save(drone);
        LOGGER.info(Const.Log.BRACKETS_5, Const.ActionType.CREATE_DRONE, Const.Log.END, Const.Log.STATUS_SUCCESS, Const.Log.DRONE, createdDrone);
        return createdDrone;
    }

    @PostMapping("/{droneId}/load")
    public @ResponseBody List<MedicationDTO> load(
            @PathVariable long droneId,
            @ModelAttribute("medicationListDTO") @Valid MedicationListDTO medicationListDTO,
            @RequestParam("images") MultipartFile[] images){
        LOGGER.info(Const.Log.BRACKETS_6, Const.ActionType.LOAD_MEDICATIONS, Const.Log.START, Const.Log.DRONE_ID, droneId, Const.Log.MEDICATIONS, medicationListDTO);
        List<MedicationDTO> loadedMedications =  droneService.load(droneId, medicationListDTO.getMedications(), images);
        LOGGER.info(Const.Log.BRACKETS_5, Const.ActionType.LOAD_MEDICATIONS, Const.Log.END, Const.Log.STATUS_SUCCESS, Const.Log.MEDICATIONS, medicationListDTO);
        return loadedMedications;
    }

    @GetMapping("/{droneId}/load")
    public @ResponseBody List<Medication> findAllLoads(@PathVariable long droneId){
        LOGGER.info(Const.Log.BRACKETS_4, Const.ActionType.FIND_ALL_LOADS, Const.Log.START, Const.Log.DRONE_ID, droneId);
        List<Medication> loadedMedications = droneService.findAllLoads(droneId);
        LOGGER.info(Const.Log.BRACKETS_5, Const.ActionType.FIND_ALL_LOADS, Const.Log.END, Const.Log.STATUS_SUCCESS, Const.Log.MEDICATIONS, loadedMedications);
        return loadedMedications;
    }

    @GetMapping("/state/{state}")
    public @ResponseBody List<Drone> findAllByState(@PathVariable Const.State state){
        LOGGER.info(Const.Log.BRACKETS_4, Const.ActionType.FIND_ALL_BY_STATE, Const.Log.START, Const.Log.STATE, state);
        List<Drone> drones =  droneService.findAllByState(state);
        LOGGER.info(Const.Log.BRACKETS_5, Const.ActionType.FIND_ALL_BY_STATE, Const.Log.END, Const.Log.STATUS_SUCCESS, Const.Log.DRONES, drones);
        return drones;
    }

    @GetMapping("/{droneId}/battery-level")
    public int getBatteryLevel(@PathVariable long droneId){
        LOGGER.info(Const.Log.BRACKETS_4, Const.ActionType.GET_BATTERY_LEVEL, Const.Log.START, Const.Log.DRONE_ID, droneId);
        int batteryLevel =  droneService.getBatteryLevel(droneId);
        LOGGER.info(Const.Log.BRACKETS_5, Const.ActionType.GET_BATTERY_LEVEL, Const.Log.END, Const.Log.STATUS_SUCCESS, Const.Log.BATTERY_LEVEL, batteryLevel);
        return batteryLevel;
    }








}
