package com.submit.drones.scheduler;

import com.submit.drones.service.DroneService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

@Configuration
@EnableScheduling
public class DroneScheduler {

    @Autowired
    DroneService droneService;

    private static final int loggingTimeInterval = 1000;

    @Scheduled(fixedDelay = loggingTimeInterval)
    public void logBatteryLevel() {
        droneService.logBatterLevel();
    }

}
