package com.submit.drones.dto;

import com.submit.drones.util.Const;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class MedicationDTO {

    private long id;
    private String name;
    private float weight;
    private String code;
    private byte[] image;
    private Const.LoadedStatus loadedStatus;

}
