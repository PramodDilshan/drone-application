package com.submit.drones.dto;

import com.submit.drones.model.Medication;
import lombok.Data;

import javax.validation.Valid;
import java.util.List;

@Data
public class MedicationListDTO {

    private List<@Valid Medication> medications;

}
