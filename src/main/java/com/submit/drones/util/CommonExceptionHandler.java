package com.submit.drones.util;


import com.submit.drones.exception.BatteryException;
import com.submit.drones.exception.DroneException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.sql.SQLIntegrityConstraintViolationException;

@RestControllerAdvice
public class CommonExceptionHandler {

    private static final Logger LOGGER = LogManager.getLogger(CommonExceptionHandler.class);

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(DroneException.class)
    public String handleDroneExceptions(DroneException exception){
        LOGGER.error(Const.Log.BRACKETS_2, Const.ActionType.HANDLE_DRONE_EXCEPTION, exception.getMessage());
        return exception.getMessage();
    }

    @ResponseStatus(HttpStatus.INSUFFICIENT_STORAGE)
    @ExceptionHandler(BatteryException.class)
    public String handleBatteryExceptions(BatteryException exception){
        LOGGER.error(Const.Log.BRACKETS_2, Const.ActionType.HANDLE_BATTERY_EXCEPTION, exception.getMessage());
        return exception.getMessage();
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(SQLIntegrityConstraintViolationException.class)
    public String handleSQLIntegrityConstraintViolationException(SQLIntegrityConstraintViolationException exception){
        LOGGER.error(Const.Log.BRACKETS_2, Const.ActionType.HANDLE_CONSTRAINT_VIOLATION, exception.getMessage());
        return exception.getMessage();
    }


}
