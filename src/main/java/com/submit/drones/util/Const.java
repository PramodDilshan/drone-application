package com.submit.drones.util;

public class Const {

    public enum Model {
        LIGHT_WEIGHT, MIDDLE_WEIGHT, CRUISE_WEIGHT, HEAVY_WEIGHT
    }

    public enum State {
        IDLE, LOADING, LOADED, DELIVERING, DELIVERED, RETURNING
    }

    public enum LoadedStatus {
        SUCCESS, FAILED
    }

    public static final class Log {

        private Log(){
        }

        public static final String START = "START";
        public static final String END = "END";

        public static final String STATUS_SUCCESS = "SUCCESS";
        public static final String STATUS_FAILED = "FAILED";

        public static final String BRACKETS_1 = "{}";
        public static final String BRACKETS_2 = "{} -> {}";
        public static final String BRACKETS_3 = "{} -> {} -> {}";
        public static final String BRACKETS_4 = "{} -> {} -> {} -> {}";
        public static final String BRACKETS_5 = "{} -> {} -> {} -> {} -> {}";
        public static final String BRACKETS_6 = "{} -> {} -> {} -> {} -> {} -> {}";

        public static final String DRONES = "DRONES";
        public static final String DRONE = "DRONE";
        public static final String DRONE_ID = "DRONE_ID";
        public static final String MEDICATIONS = "MEDICATIONS";
        public static final String STATE = "STATE";
        public static final String BATTERY_LEVEL = "BATTERY_LEVEL";
    }

    public static class ErrorMessages {

        private ErrorMessages(){
        }

        public static final String NO_DRONES_ARE_FOUND = "No drones are found";
        public static final String PLEASE_CHARGE_THE_BATTERY_BEFORE_LOADING = "Please charge the battery before loading";
        public static final String NO_MEDICATIONS_FOUND = "No Medications are Found";
    }

    public enum ActionType {
        FIND_ALL_DRONES, CREATE_DRONE, LOAD_MEDICATIONS, FIND_ALL_LOADS, FIND_ALL_BY_STATE, CREATE, GET_BATTERY_LEVEL,
        UPDATE, FIND_ALL, LOAD, UPDATE_STATE, VALIDATE_MAX_WEIGHT, SAVE_ALL, SAVE, HANDLE_DRONE_EXCEPTION, HANDLE_BATTERY_EXCEPTION,
        SET_IMAGES_TO_MEDICATIONS, HANDLE_CONSTRAINT_VIOLATION
    }

}
