package com.submit.drones.exception;

public class DroneException extends RuntimeException{

    public DroneException(String message){
        super(message);
    }
}
