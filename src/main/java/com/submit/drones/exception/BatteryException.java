package com.submit.drones.exception;

import com.submit.drones.util.Const;

public class BatteryException extends RuntimeException {

    public BatteryException() {
        super(Const.ErrorMessages.PLEASE_CHARGE_THE_BATTERY_BEFORE_LOADING);
    }
}
