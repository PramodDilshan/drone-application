package com.submit.drones.repository;

import com.submit.drones.model.Drone;
import com.submit.drones.util.Const;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface DroneRepository extends JpaRepository<Drone, Long> {

    Optional<List<Drone>> findByState(Const.State state);
}
