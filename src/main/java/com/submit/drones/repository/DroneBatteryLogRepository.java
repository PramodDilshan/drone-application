package com.submit.drones.repository;

import com.submit.drones.model.DroneBatteryLog;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DroneBatteryLogRepository extends JpaRepository<DroneBatteryLog, Long> {

}
