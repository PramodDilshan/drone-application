package com.submit.drones.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.submit.drones.util.Const;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.List;


@Data
@Entity(name = "drone")
public class Drone{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "drone_id_sequence")
    @SequenceGenerator(name = "drone_id_sequence", initialValue = 11)
    private long id;

    @Size(max = 100)
    @NotBlank(message = "serial number cannot be blank")
    @Column(unique = true)
    private String serialNumber;

    @Enumerated(EnumType.STRING)
    private Const.Model model;

    @Max(500)
    @Column(scale = 2)
    private float weightLimit;

    @Max(100)
    private int batteryCapacity;

    @Enumerated(EnumType.STRING)
    private Const.State state = Const.State.IDLE;

    @JsonManagedReference
    @OneToMany(mappedBy = "drone", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<Medication> load;

    @Override
    public String toString(){
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Drone (id=");
        stringBuilder.append(id);
        stringBuilder.append(", serial number=");
        stringBuilder.append(serialNumber);
        stringBuilder.append(", model=");
        stringBuilder.append(model);
        stringBuilder.append(", weight limit=");
        stringBuilder.append(weightLimit);
        stringBuilder.append(", battery capacity=");
        stringBuilder.append(batteryCapacity);
        stringBuilder.append(", state=");
        stringBuilder.append(state);
        stringBuilder.append(")");
        return stringBuilder.toString();
    }
}
