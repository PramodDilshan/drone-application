package com.submit.drones.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;


@Data
@Entity(name = "medication")
public class Medication{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Pattern(regexp = "^[A-Za-z0-9_-]*$", message = "name is allowed only letters, numbers, underscore and dash")
    @NotBlank(message = "name cannot be blank")
    private String name;

    @Max(500)
    @Column(scale = 2)
    private float weight;

    @Pattern(regexp = "^[A-Z0-9_]*$", message = "name is allowed only upper case letters, numbers and underscore")
    @NotBlank(message = "name cannot be blank")
    @Column(unique = true)
    private String code;

    @Lob
    private byte[] image;

    @JsonBackReference
    @ManyToOne
    @JoinColumn
    private Drone drone;

    @Override
    public String toString(){
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Medication (id=");
        stringBuilder.append(id);
        stringBuilder.append(", name=");
        stringBuilder.append(name);
        stringBuilder.append(", weight=");
        stringBuilder.append(weight);
        stringBuilder.append(", code=");
        stringBuilder.append(code);
        stringBuilder.append(", drone id=");
        stringBuilder.append(drone.getId());
        stringBuilder.append(")");
        return stringBuilder.toString();
    }
}
