package com.submit.drones.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import javax.validation.constraints.Max;
import java.util.Date;

@Data
@Entity
@NoArgsConstructor
@EntityListeners(AuditingEntityListener.class)
public class DroneBatteryLog {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @JsonBackReference
    @ManyToOne
    @JoinColumn
    private Drone drone;

    @CreatedDate
    private Date createdTimeStamp;

    @Max(100)
    private int batteryCapacity;

    public DroneBatteryLog(Drone drone, int batteryCapacity) {
        this.drone = drone;
        this.batteryCapacity = batteryCapacity;
    }
}
