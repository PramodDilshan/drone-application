# README

### Dependencies
Gradle:7.5.1
Java:11


### Build Instruction
Use following command at project root path to build .jar file. You can use globally installed gradle platform of project specified gradle platform. 
.jar file will be created at /build/libs/drones-1.0.0.jar

1. Use globally installed gradle platform
gradle clean build    

2. Use project specified gradle platform
./gradlew clean build


### Run Instruction
Use following command at .jar file location to run the build file.
REST service will be run at http://localhost:8080/demo-app

java -jar drones-1.0.0.jar


### REST API Test Instruction

1. registering a drone

   curl --location --request POST 'http://localhost:8080/demo-app/drone' \
   --header 'Content-Type: application/json' \
   --data-raw '{
   "serialNumber": "SERIAL_000110",
   "model": "MIDDLE_WEIGHT",
   "weightLimit": 200,
   "batteryCapacity": 90,
   "state": "IDLE"
   }'

2. loading a drone with medication items

   curl --location --request POST 'http://localhost:8080/demo-app/drone/{droneId}/load' \
   --form 'medications[0].name="MED_NAME_01"' \
   --form 'medications[0].weight="100.0"' \
   --form 'medications[0].code="CODE_000110"' \
   --form 'images=@"/path/to/images/image1.jpeg"' \
   --form 'medications[1].name="MED_NAME_02"' \
   --form 'medications[1].weight="100.1"' \
   --form 'medications[1].code="CODE_000120"' \   
   --form 'images=@"/path/to/images/image2.jpeg"'

3. checking loaded medication items for a given drone

   curl --location --request GET 'http://localhost:8080/demo-app/drone/{droneId}/load'

4. checking available drones for loading 

   curl --location --request GET 'http://localhost:8080/demo-app/drone/state/IDLE'

5. check drone battery level for a given drone

   curl --location --request GET 'http://localhost:8080/demo-app/drone/{droneId}/battery-level'

6. Find all drones

   curl --location --request GET 'http://localhost:8080/demo-app/drone'


### Database 
Local H2 database will be created and preloaded 10 drones.

URL: jdbc:h2:mem:mydb
Username: sa
Password: password
Preloaded SQL script file: /resources/data.sql


### Tables

1. DRONE
2. MEDICATION
3. DRONE_BATTERY_LOG


### Property Constraint and Assumption

Drone
1. id - unique, auto generated, initial value will be 11 as there are 10 preloaded data.
2. serial number - 100 max characters, cannot be blank, should be unique
3. model - should be LIGHT_WEIGHT or MIDDLE_WEIGHT or CRUISE_WEIGHT or HEAVY_WEIGHT
4. weight limit - 500 (g) max, decimal values can be used upto 2 scale
5. battery capacity - 100 (%) max, 
6. state - should be IDLE or LOADING or LOADED or DELIVERING or DELIVERED or RETURNING, default value is IDLE

Medication
1. id - unique, auto generated
2. name - allowed only letters, numbers, ‘-‘, ‘_’, cannot be blank
3. weight - 500 (g) max as thats the drone's max capacity, decimal values can be used upto 2 scale
4. code - allowed only upper case letters, '_' and numbers, cannot be blank, should be unique
5. image - will be saved as byte array in the database
6. drone id - contained drone's reference id 

Drone Battery Log
1. id - unique, auto generated
2. battery capacity - 100 (%) max
3. created timestamp - logged data and time
4. drone id - drone's reference id



